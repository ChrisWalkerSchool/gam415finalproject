// Fill out your copyright notice in the Description page of Project Settings.

#include "WhiteboardActor.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "Engine.h"

// Sets default values
AWhiteboardActor::AWhiteboardActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//This section of code first initializes the StaticMeshComponent and sets it to be the RootComponent for the actor before using Constructor Helpers to
	//Actually change that static mesh to use the one included in the plugin files (Which is just a copy of the basic 1M Cube Chamfer mesh).
	whiteboard_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Whiteboard_Mesh"));
	RootComponent = whiteboard_mesh;
	static ConstructorHelpers::FObjectFinder<UStaticMesh>meshAsset(TEXT("StaticMesh'/WhiteboardPlugin/WhiteboardFiles/Whiteboard_Cube.Whiteboard_Cube'"));
	whiteboard_mesh->SetStaticMesh(meshAsset.Object);

	//Creates an object out of the whiteboard material and then sets the Mesh's material to it.
	static ConstructorHelpers::FObjectFinder<UMaterial>matAsset(TEXT("Material'/WhiteboardPlugin/WhiteboardFiles/M_Whiteboard.M_Whiteboard'"));
	whiteboard_mesh->SetMaterial(0, matAsset.Object);

	//Creates the pointer to the Marker Material, one-half of the dynamic material that will ultimately help the player draw on the cube.
	static ConstructorHelpers::FObjectFinder<UMaterial>markerAsset(TEXT("Material'/WhiteboardPlugin/WhiteboardFiles/M_Marker.M_Marker'"));
	markerMaterial = markerAsset.Object;

	//Creates the pointer to the RenderTarget object needed by the whiteboard for the player's drawings to actually show up.
	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D>RTAsset(TEXT("TextureRenderTarget2D'/WhiteboardPlugin/WhiteboardFiles/RT_Whiteboard.RT_Whiteboard'"));
	whiteboardRT = RTAsset.Object;
}

// Called when the game starts or when spawned
void AWhiteboardActor::BeginPlay()
{
	Super::BeginPlay();
	
	//First checks to make sure that the pointer to the Marker's Material was properly created before creating the Instance of the Dynamic Material.
	if (markerMaterial)
	{
		dynamicMarkerMaterial = UMaterialInstanceDynamic::Create(markerMaterial, this);
	}

	//Clears the RenderTarget attached to this actor; removing anything that may have been drawn on it in a previous game.
	UKismetRenderingLibrary::ClearRenderTarget2D(this, whiteboardRT, FLinearColor::Black);

	//Sets the DrawSize parameter in the dynamic material to that of the variable. While nothing does so currently; this does allow other parts of the game
	//to adjust the stroke size being used to draw on the whiteboard cubes.
	dynamicMarkerMaterial->SetScalarParameterValue("DrawSize", drawSize);

}

// Called every frame
void AWhiteboardActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//This is the function that is used to actually draw where the player is pointing.
void AWhiteboardActor::DrawOnWhiteboard(FVector2D spotToDraw)
{
	
	//When this function receives a FVector2D, it builds a LinearColor out of a static 0 and the X and Y values from the vector.
	//Basically, the dynamic material uses the LinearColor as a means of storing the X and Y cooridinates of where the player is drawing.
	dynamicMarkerMaterial->SetVectorParameterValue("DrawLocation", FLinearColor(spotToDraw.X, spotToDraw.Y, 0));

	//The Dynamic Material is then passed into a function from the RenderingLibrary, along with the RenderTarget, which draws a preset color
	//onto the surface of the cube at whatever point was passed in.
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(this, whiteboardRT, dynamicMarkerMaterial);
}

