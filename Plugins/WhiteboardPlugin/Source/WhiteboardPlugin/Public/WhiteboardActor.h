// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WhiteboardActor.generated.h"

UCLASS()
class WHITEBOARDPLUGIN_API AWhiteboardActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWhiteboardActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Declares a UPROPERTY for the StaticMeshComponent that acts as the cube the player can draw on. Given the EditAnywhere and BlueprintReadWrite flags
	//so the derived blueprint of this C++ class can replace the basic material with a dynamic one.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* whiteboard_mesh;

	//The next two UPROPERTIES are used to define the two parts needed for a C++ dynamic material; a UMaterialInterface and a MaterialInstanceDynamic.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* markerMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstanceDynamic* dynamicMarkerMaterial;

	//Defines the UPROPERTY for the RenderTarget, which is necessary for the actual act of drawing.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextureRenderTarget2D* whiteboardRT;

	//Defines a UPROPERTY float that can be used to adjust the size of the circles drawn on the cube. Not currently used, but can be implemented later.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float drawSize;
	
	//Defines a UFUNCTION that is actually what determines the location on the cube to draw on and applies a circle of color at that spot.
	UFUNCTION(BlueprintCallable)
		void DrawOnWhiteboard(FVector2D spotToDraw);
};
