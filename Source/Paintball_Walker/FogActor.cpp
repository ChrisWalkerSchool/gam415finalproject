// Fill out your copyright notice in the Description page of Project Settings.

#include "FogActor.h"
#include "Engine.h"

// Sets default values
AFogActor::AFogActor() :m_wholeTextureRegion(0, 0, 0, 0, m_textureSize, m_textureSize)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	m_coverSize = 5000;

	//Create a plane mesh from the basic static mesh component
	m_squarePlane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FOW Plane Static Mesh"));
	RootComponent = m_squarePlane;

	//This bit of code sets up the collision rules for the plane and finds a specific static mesh to use for it from within the Engine files.
	m_squarePlane->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	{
		static ConstructorHelpers::FObjectFinder<UStaticMesh> asset(TEXT("/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane"));
		m_squarePlane->SetStaticMesh(asset.Object);
	}

	//These lines set up a few more properties of the static mesh before the code moves on.
	m_squarePlane->TranslucencySortPriority = 100;
	m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));


	//This quickly creates a pointer to the FogMaterial that will be used for the square plane, though a dynamic material will be set up for it later.
	{
		static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/Fog/FogMaterial.FogMaterial'"));
		m_dynamicMaterial = asset.Object;
	}

	//This if statement creates the dynamic texture if one does not already exist.
	if (!m_dynamicTexture) {
		m_dynamicTexture = UTexture2D::CreateTransient(m_textureSize, m_textureSize, PF_G8);
		m_dynamicTexture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		m_dynamicTexture->SRGB = 0;
		m_dynamicTexture->UpdateResource();
		//m_dynamicTexture->MipGenSettings = TMGS_NoMipmaps;
		/*For whatever reason, Visual Studio is convinced that "MipGenSettings" is not part of UTexture2D even though I can auto-complete it in.
		At the very least, commenting out this line doesn't seem to have any effect on the overall function of FogActor, so I'm not losing anything by doing so.*/
	}

	//Initializes the pixel array to all solid pixels.
	for (int x = 0; x < m_textureSize; ++x)
	{
		for (int y = 0; y < m_textureSize; ++y)
		{
			m_pixelArray[y * m_textureSize + x] = 255;
		}
	}

	//Updates the actual texture on the square plane based on information from elsewhere in the actor.
	if (m_dynamicTexture) UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
}

//When this function is called, it will create a dynamic material instance from the FogMaterial set up in Unreal and then set the square plane to use it.
//Without doing so, the appearance of the plane would never change no matter how much the player moved around.
void AFogActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	// Create a dynamic material instance to swap in the fog texture.  
	if (m_dynamicMaterial) {
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
		m_dynamicMaterialInstance->SetTextureParameterValue("FogTexture", m_dynamicTexture);
	}
	// Set the dynamic material to the mesh.  
	if (m_dynamicMaterialInstance) m_squarePlane->SetMaterial(0, m_dynamicMaterialInstance);

}

//A simple function to set the size of the FogActor in the world as a large flat plane.
void AFogActor::setSize(float s)
{
	m_coverSize = s;
	m_squarePlane->SetRelativeScale3D(FVector(m_coverSize, m_coverSize, 1));
}

//This function is used to determine both where the player currently is within the "grid" of pixels that make up the fog actor
//and for determining the circle in which those pixels should be cleared.
void AFogActor::revealSmoothCircle(const FVector2D & pos, float radius)
{
	//All of this is the math that determines where the circle's center point and edges are.
	FVector2D texel = pos - FVector2D(GetActorLocation().X, GetActorLocation().Y);
	texel = texel * m_textureSize / m_coverSize;
	texel += FVector2D(m_textureSize / 2, m_textureSize / 2); // Calculate radius in texel unit ( 1 is 1 pixel )  
	float texelRadius = radius * m_textureSize / m_coverSize; // The square area to update  
	int minX = FMath::Clamp <int>(texel.X - texelRadius, 0, m_textureSize - 1);
	int minY = FMath::Clamp <int>(texel.Y - texelRadius, 0, m_textureSize - 1);
	int maxX = FMath::Clamp <int>(texel.X + texelRadius, 0, m_textureSize - 1);
	int maxY = FMath::Clamp <int>(texel.Y + texelRadius, 0, m_textureSize - 1);
	uint8 theVal = 0; // Update our array of fog value in memory  

	//And this is the for loop that goes through all of the pixels of that circle and clears them of their color. Assuming any pixels were changed, this for loop will
	//also set up a boolean to trigger the redraw of the fog (and actually make the pixels disappear) once it's finished.
	bool dirty = false;
	for (int x = minX; x < maxX; ++x) {
		for (int y = minY; y < maxY; ++y) {
			float distance = FVector2D::Distance(texel, FVector2D(x, y));
			if (distance < texelRadius) {
				static float smoothPct = 0.7f;
				uint8 oldVal = m_pixelArray[y * m_textureSize + x];
				float lerp = FMath::GetMappedRangeValueClamped(FVector2D(smoothPct, 1.0f), FVector2D(0, 1), distance / texelRadius);
				uint8 newVal = lerp * 255;
				newVal = FMath::Min(newVal, oldVal);
				m_pixelArray[y * m_textureSize + x] = newVal;
				dirty = dirty || oldVal != newVal;
			}
		}
	} // Propagate the values in memory's array to texture.  
	if (dirty) UpdateTextureRegions(m_dynamicTexture, 0, 1, &m_wholeTextureRegion, m_textureSize, 1, m_pixelArray, false);
}

//I honestly have no idea how this function works, but this is what is ultimately responsible for "clearing" away the black fog as the player moves around.
void AFogActor::UpdateTextureRegions(UTexture2D * Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D * Regions, uint32 SrcPitch, uint32 SrcBpp, uint8 * SrcData, bool bFreeData) {
	if (Texture->Resource) {
		struct FUpdateTextureRegionsData {
			FTexture2DResource * Texture2DResource;
			int32 MipIndex;
			uint32 NumRegions;
			FUpdateTextureRegion2D * Regions;
			uint32 SrcPitch;
			uint32 SrcBpp;
			uint8 * SrcData;
		};
		FUpdateTextureRegionsData * RegionData = new FUpdateTextureRegionsData;
		RegionData->Texture2DResource = (FTexture2DResource *)Texture -> Resource;
		RegionData->MipIndex = MipIndex;
		RegionData->NumRegions = NumRegions;
		RegionData->Regions = Regions;
		RegionData->SrcPitch = SrcPitch;
		RegionData->SrcBpp = SrcBpp;
		RegionData->SrcData = SrcData;
		ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(UpdateTextureRegionsData, FUpdateTextureRegionsData *, RegionData, RegionData, bool, bFreeData, bFreeData, {
			for (uint32 RegionIndex = 0; RegionIndex < RegionData->NumRegions; ++RegionIndex) {
				int32 CurrentFirstMip = RegionData->Texture2DResource->GetCurrentFirstMip();
				if (RegionData->MipIndex >= CurrentFirstMip) {
					RHIUpdateTexture2D(RegionData->Texture2DResource->GetTexture2DRHI(), RegionData->MipIndex - CurrentFirstMip, RegionData->Regions[RegionIndex], RegionData->SrcPitch, RegionData->SrcData + RegionData->Regions[RegionIndex].SrcY * RegionData->SrcPitch + RegionData->Regions[RegionIndex].SrcX * RegionData->SrcBpp);
				}
			}
			if (bFreeData) {
				FMemory::Free(RegionData->Regions);
				FMemory::Free(RegionData->SrcData);
			}
			delete RegionData;
			});
	}

}

// Called when the game starts or when spawned
void AFogActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFogActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

