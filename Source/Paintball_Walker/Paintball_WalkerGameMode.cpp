// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_WalkerGameMode.h"
#include "Paintball_WalkerHUD.h"
#include "Paintball_WalkerCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintball_WalkerGameMode::APaintball_WalkerGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_WalkerHUD::StaticClass();
}
