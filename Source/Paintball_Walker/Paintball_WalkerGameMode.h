// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paintball_WalkerGameMode.generated.h"

UCLASS(minimalapi)
class APaintball_WalkerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintball_WalkerGameMode();
};



