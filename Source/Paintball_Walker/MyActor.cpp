// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"
#include "ProceduralMeshComponent.h"


// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates a ProceduralMeshComponent subobject and sets that to be the root component of the actor.
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//This function is called once the actor has finished spawning into the world, and then goes on to call the base Actor's function before creating a 2D square.
void AMyActor::PostActorCreated() {
	Super::PostActorCreated();
	CreateSquare();
}

//This function is called in different circumstances compared to PostActorCreated, but otherwise works the same.
void AMyActor::PostLoad() {
	Super::PostLoad();
	CreateSquare();
}

//This function is used to actually create the square.
void AMyActor::CreateSquare() {
	//It starts by creating most of the arrays needed to contain the information about the square.
	TArray <FVector> Vertices;
	TArray <int32> Triangles;
	TArray <FVector> Normals;
	TArray <FLinearColor> Colors;

	//It then goes on to define the four vertices (corners) of said square, and then loading a sequence into Triangles indicating which vertices are used for each triangle.
	//In this case, the first triangle (half of the square) is drawn between vertices 0, 1, and 2, while the second is drawn between the points 3, 2, and 1.
	Vertices.Add(FVector(0.f, 0.f, 0.f));
	Vertices.Add(FVector(0.f, 100.f, 0.f));
	Vertices.Add(FVector(0.f, 0.f, 100.f));
	Vertices.Add(FVector(0.f, 100.f, 100.f));
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(3);
	Triangles.Add(2);
	Triangles.Add(1);

	//A for loop is used to fill the other two arrays for convenience. They just need multiple of the same value, so this is easier than writing the same line multiple times.
	for (int32 i = 0; i < Vertices.Num(); i++) {
		Normals.Add(FVector(0.f, 0.f, 1.f));
		Colors.Add(FLinearColor::Red);
	}

	//Declares the last two arrays needed to create the mesh, but because these are unimportant for a 2D object they're left empty.
	TArray <FVector2D> UV0;
	TArray <FProcMeshTangent> Tangents;

	//Finally creates the square by calling the CreateMeshSection function and passing it all of the arrays along with a few other values.
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, Colors, Tangents, true);
}