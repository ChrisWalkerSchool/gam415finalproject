// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "FogActor.generated.h"

UCLASS()
class PAINTBALL_WALKER_API AFogActor : public AActor
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	AFogActor();

	virtual void PostInitializeComponents() override;

	// Sets the fog's size.
	void setSize(float s);

	//Reveal a portion of the fog
	void revealSmoothCircle(const FVector2D &pos, float radius);


private:
	//Declares the function that will actually update the appearance of the square plane used to display the fog of war.
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

	//Declares a const int used to make math around the changing texture easier to write.
	static const int m_textureSize = 512;

	//Declares four UPROPERTIES that are each used to hold a component critical for the dynamic material to actually work
	UPROPERTY()
		UStaticMeshComponent* m_squarePlane;
	UPROPERTY()
		UTexture2D* m_dynamicTexture;
	UPROPERTY()
		UMaterialInterface* m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic* m_dynamicMaterialInstance;

	//Declares an array used to hold information about each of the pixels that make up the texture on the fog of war plane.
	uint8 m_pixelArray[m_textureSize * m_textureSize];

	//Declares the last few bits of information necessary for the dynamic material to work as intended.
	FUpdateTextureRegion2D m_wholeTextureRegion;

	float m_coverSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
