// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"  //includes the RuntimeMeshComponent header so the object can successfully build and be used.
#include "RuntimeCubeActor.generated.h"

UCLASS()
class PAINTBALL_WALKER_API ARuntimeCubeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuntimeCubeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Declares two functions for this actor class; one that will be used to outline the details of the cube and the other to generate the cube based on that information.
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> &Vertices, TArray <int32> &Triangles, TArray <FVector> &Normals, TArray <FVector2D> &UVs, TArray <FRuntimeMeshTangent> &Tangents, TArray <FColor> &Colors);

private://Declares one private UPROPERTY; the RuntimeMeshComponent that ultimately will take the shape of a cube.
	UPROPERTY(VisibleAnywhere)
	URuntimeMeshComponent* mesh;
	
};
